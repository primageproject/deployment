
To deploy the Jupyter Notebooks service, load the file "deploy_iteractive-jupyter.yaml" in Kubernetes dashboard.  
At least you must change the "SOME_SECRET_TOKEN".

Then the service can be accessed in that case (PRIMAGE platform and node port 30051) through the URL:  
    http://upv.datahub.egi.eu:30051/

You will have to enter the secret token mentioned previously.

Then you can upload and open the notebook "htc-combine.ipynb".
