# Batch job


First of all, you need to build the dataset. This can be done using the Python script available in the Scripts folder, which also contains the script for training. As this is a validation test of the infrastructure, we train the network only five epochs.

The files _configmap_backend.yaml_, _configmap_job-breasttumour.yaml_ and _hpc.connector-job-breasttumour.yaml_ contains, respectively, the configuration of the connection for the Prometheus, the job configuration of the job that will be submitted using HPC-Connector to Prometheus, and the job configuration of HPC-connector (which is executed in the Kubernetes cluster). 

We also added the model (_breast-tumour-model.h5_), the logs of the HPC-connector job (_logs-from-hpc-connector-in-breast-tumour-b62mg.txt_) obtained from Kubernetes and the logs of the job executed in Prometheus (_slurm-24117992.out_). Besides, you can see at the HPC-connector logs that HPC-Connector also get the logs when the SLURM job is finished.