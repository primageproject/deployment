#!/usr/bin/env python
import os
import sys
import redis_primage_htc_queue
import datetime
import pandas as pd

primage_htc_batch_queue_service = sys.argv[1]  #First argument corresponds to the Queue service ($PRIMAGE_HTC_BATCH_QUEUE_SERVICE)
queue_name                      = sys.argv[2]  #Second argument corresponds to the name of the queue ($QUEUE_NAME)
TEST_PATH                       = sys.argv[3]       # Example: "datasets/idc/testing"
PARTITION_SIZE                  = int(sys.argv[4])  # Number of images per job. Example: 5000

#First, the producer creates a Queue named $QUEUE_NAME from PRIMAGE HTC Queue Service (value from $PRIMAGE_HTC_BATCH_QUEUE_SERVICE).
primage_htc_queue = redis_primage_htc_queue.RedisWQ(name=queue_name, host=primage_htc_batch_queue_service)
print("Push with sessionID: " +  primage_htc_queue.sessionID())
print("Initial queue state: empty=" + str(primage_htc_queue.empty()))

def generate_partition(files, classes, n_partition, partitions_dir):
    partition_file = os.path.join(partitions_dir, "p{}.csv".format(n_partition))
    print('- Writing ' + partition_file)
    df = pd.DataFrame({"file": files, "class": classes}, dtype="string")
    df.to_csv(partition_file)
    print('- Pushing to the queue: ' + partition_file)
    primage_htc_queue.push(partition_file)


PARTITIONS_DIR_PREFIX = "partitions"
timestamp = datetime.datetime.now().strftime("%y%m%d-%H%M%S")
partitions_dir = os.path.join(TEST_PATH, "%s_%s" % (PARTITIONS_DIR_PREFIX, timestamp))
os.makedirs(partitions_dir)

n_partition = 0
files = []
classes = []
for class_dir in os.listdir(TEST_PATH):
    if class_dir.startswith(PARTITIONS_DIR_PREFIX): continue
    for image_file in os.listdir(os.path.join(TEST_PATH, class_dir)):
        files.append(os.path.join(class_dir, image_file))
        classes.append(class_dir)
        if len(files) == PARTITION_SIZE: 
            generate_partition(files, classes, n_partition, partitions_dir)
            n_partition += 1
            files = []
            classes = []
if len(files) > 0:
    generate_partition(files, classes, n_partition, partitions_dir)
print("Queue completed")
