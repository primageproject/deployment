
First, the python code (the two .py files) must be uploaded to the user persistent home directory.  
You can put them in the same directory where there are the "datasets" directory and .h5 file of trained model.  
This paths can be changed in the YAML files.


To deploy the Queue Service and the jobs, load each deployment YAML file in Kubernetes dashboard.

The order should be:
 - deploy_htc-queue-service.yaml
 - deploy_htc-batch-producer-job.yaml
 - deploy_htc-batch-consumer-jobs.yaml

In that case all the results will be written in:
    "/persistent-home/datasets/idc/testing/partitions_\<timestamp\>/"

The producer writes one .csv file for each partition.  
The consumers write one .out.npy file for each partition.
