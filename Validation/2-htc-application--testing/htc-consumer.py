#!/usr/bin/env python
import os
import sys
import redis_primage_htc_queue
import pandas as pd
import tensorflow as tf
import numpy as np

BS=32
PARTITIONS_DIR_NAME = "partitions"

def process_partition(model, img_generator, TEST_PATH, partition_file):
    print('- Reading ' + partition_file)
    df = pd.read_csv(partition_file)
    df["class"] = df["class"].astype("string")
    n_rows, n_cols = df.shape
    testGen = img_generator.flow_from_dataframe(
        df,
        directory=TEST_PATH,
        x_col="file",
        y_col="class",
        class_mode="categorical",
        target_size=(48, 48),
        color_mode="rgb",
        shuffle=False,
        batch_size=BS)
    testGen.reset()
    print('- Predicting for partition ' + partition_file)
    predIdxs = model.predict(x=testGen, steps=(n_rows // BS) + 1)
    predIdxs = np.argmax(predIdxs, axis=1)
    outfile = partition_file.split('.')[0] + ".out"
    print('- Writing ' + outfile)
    np.save(outfile, predIdxs)


primage_htc_batch_queue_service = sys.argv[1] #"PRIMAGE_HTC_BATCH_QUEUE_SERVICE"
queue_name                      = sys.argv[2] #"QUEUE_NAME"
MODEL_FILE_NAME                 = sys.argv[3]       # Example: "breast-tumour-model.h5"
TEST_PATH                       = sys.argv[4]       # Example: "datasets/idc/testing"

#First, the consumer connect to the Queue named $QUEUE_NAME from PRIMAGE HTC Queue Service (values from $PRIMAGE_HTC_BATCH_QUEUE_SERVICE).
primage_htc_queue = redis_primage_htc_queue.RedisWQ(name=queue_name, host=primage_htc_batch_queue_service)
print("Push with sessionID: " +  primage_htc_queue.sessionID())
print("Initial queue state: empty=" + str(primage_htc_queue.empty()))

# initialize the data augmentation object
img_generator = tf.keras.preprocessing.image.ImageDataGenerator(rescale=1 / 255.0)
# load the model from file
model = tf.keras.models.load_model(MODEL_FILE_NAME)

while not primage_htc_queue.empty():
    item = primage_htc_queue.lease(lease_secs=80, block=True, timeout=2) 
    if item is not None:
        partition_file = item.decode("utf-8")
        print("Working on item: " + partition_file)
        process_partition(model, img_generator, TEST_PATH, partition_file)
        primage_htc_queue.complete(item)
    else:
        print("Queue empty waiting for work")
        
print("Queue empty, exiting")
